<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['prefix' => 'admin', 'middleware' => ['auth' => 'admin']], function () {
//     Route::get('/home', function(){
//         return view('welcome');
//     });
// });

Route::namespace('Admin')->group(function () {
    Route::get('/admin', 'AdminController@index')->middleware('admin')->name('admin.index');
    Route::get('/admin/categories/create', 'AdminCategoryController@create')->middleware('admin')->name('categories.create');
    Route::get('/admin/categories', 'AdminCategoryController@index')->name('categories.index');
    Route::get('/admin/category/{id}', 'AdminCategoryController@show')->name('categories.show');
    Route::get('/admin/categories/{categoryId}/edit', 'AdminCategoryController@edit')->name('categories.edit');
    Route::put('/admin/categories/{category}', 'AdminCategoryController@update')->name('categories.update');
    Route::post('/admin/category', 'AdminCategoryController@store')->middleware('admin')->name('categories.store');
    Route::delete('/admin/category/{id}', 'AdminCategoryController@destroy')->name('categories.destroy');

    Route::get('/admin/games', 'AdminGameController@index')->name('games.index');
    Route::get('/admin/games/create', 'AdminGameController@create')->middleware('admin')->name('admin.game.create');
    Route::post('/admin/game', 'AdminGameController@store')->middleware('admin')->name('admin.game.store');
    Route::get('/admin/games/{id}', 'AdminGameController@show')->name('admin.game.show');
    Route::get('/admin/games/{gameId}/edit', 'AdminGameController@edit')->name('admin.game.edit');
    Route::put('/admin/games/{game}', 'AdminGameController@update')->name('admin.game.update');
    Route::delete('/admin/games/{id}', 'AdminGameController@destroy')->name('admin.game.destroy');
    Route::delete('/admin/gameImage/{id}', 'AdminGameController@destroyImages')->name('admin.game.destroyImages');

    Route::get('/admin/users', 'AdminUserController@index')->name('admin.users.index');
    Route::get('/admin/users/{id}', 'AdminUserController@show')->name('admin.users.show');
    Route::delete('/admin/users/{id}', 'AdminUserController@destroy')->name('admin.users.destroy');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'UserController@index')->middleware('verified');

Route::get('/categories', 'CategoryController@index');

Route::get('/users/{user}', 'UserController@show');
Route::get('/user/edit-profile', 'UserController@editProfile')->name('users.edit_profile');
Route::get('/user/view-games', 'UserController@viewGames')->name('users.view_games');
Route::put('/users', 'UserController@updateProfile')->name('users.update_profile');
Route::post('/user/update-password', 'UserController@updatePassword')->name('users.change_password');
Route::post('/user-add-game/{game}', 'UserController@addGame')->name('user_game.store');
Route::post('/user-remove-game/{game}', 'UserController@removeGame')->name('user_game.remove');
Route::post('/user-add-friend/{user}', 'UserController@getaddFriend')->name('users.add_friend');
Route::post('/user-remove-friend/{user}', 'UserController@getremoveFriend')->name('users.remove_friend');
Route::get('/user/view-friends', 'UserController@viewFriends')->name('users.view_friends');
Route::post('/user/{conversationId}', 'UserController@sendMessage')->name('users.send_message');
// Route::post('/user-conversation-messages', 'UserController@viewMessages')->name('users.view_conversation_messages');
Route::get('/user/view-all-conversations', 'UserController@viewAllConversations')->name('users.view_all_conversations');
Route::get('/user/view-conversation/{conversationId}', 'UserController@viewCurrentConversation')->name('users.view_current_conversation');

Route::get('/user-send-message/{receiverId}', 'ConversationController@checkForExistingConversation')->name('conversations.check_conversation');

Route::get('/games', 'GameController@index');
Route::get('/games-images', 'GameImagesController@index');
Route::get('/games/{game}', 'GameController@show');
Route::get('/games/show-game/{game}', 'GameController@showGame')->name('games.show_game');