@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="text-center mb-5 mt-3">All Game Categories</h1>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
            </tr>
        </thead>
        <tbody>
             @foreach($categories as $category)
              <tr>
                  <td scope="row"><a href="/games?cat={{$category->id}}" class="text-decoration-none">{{$category->id}}</a>  </td>
                  <td scope="row"><a href="/games?cat={{$category->id}}" class="text-decoration-none"> {{$category->category_name}}</a> </td>
                  <td scope="row"><img class="img-fluid" src={{ asset('images/categories/'.$category->image) }} alt="{{$category->category_name}}"> </td>
              </tr>
             @endforeach
       </tbody>
    </table>
    {{ $categories->links() }}
</div>

@endsection
