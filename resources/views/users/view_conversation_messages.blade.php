@extends('layouts.app')
@section('content')
<div class="container">
    <div class="text-center">
        Messages with...
    </div>
    @foreach ($messages as $message)
        @if ($message->sender_id == Auth::user()->id)
        <div class="text-left p-2 bg-primary text-white">
               {{ Auth::user()->username }}:<br> {{ $message->message }}
        </div>
        @else
        <div class="text-right p-2 bg-dark text-white">
                {{ $other_correspondent->username }}: <br> {{ $message->message }}
        </div>
        @endif
        @endforeach
   <div>
        
   </div>
</div>

@endsection
