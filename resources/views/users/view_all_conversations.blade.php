@extends('layouts.app')
@section('content')
<div class="container">
    <div class="text-center">
        <h3>Conversations</h3>
    </div>
    @foreach ($allConversations as $conversation)

        @if ($conversation->user_one != Auth::user()->id)
        <div class="text-left p-2 bg-secondary text-white">
             <a href="{{ route('users.view_current_conversation',  $conversation) }}" class="btn btn-primary">{{ $conversation->getOtherCorrespondent(Auth::user())->username }}</a>
        </div>
        @else
        <div class="text-right p-2 bg-dark text-white">
             <a href="{{ route('users.view_current_conversation',  $conversation) }}" class="btn btn-primary">{{ $conversation->getOtherCorrespondent(Auth::user())->username }}</a>
        </div>
        @endif
        @endforeach
   <div>
        
   </div>
</div>

@endsection
