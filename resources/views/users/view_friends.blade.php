@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">All Friends Of {{  $user->username }}</h1>
    </div>
  
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Username</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
                <th scope="col"> Messages </th>
            </tr>
        </thead>
        <tbody>
             @foreach($userFriends as $user)
                <tr>
                    <td scope="row"><a href="/users/{{ $user->id }}" class="text-decoration-none">{{ $user->id }}</a></td>
                    <td scope="row"><a href="/users/{{ $user->id }}" class="text-decoration-none">{{ $user->name }}</a></td>
                    <td scope="row"><a href="/users/{{ $user->id }}" class="text-decoration-none">{{ $user->username }}</a></td>
                    <td scope="row"><img class ='img-fluid' src={{ $user->getImage()}} alt="{{$user->name}}"> </td>
                    <td scope='row'>
                        <form action="{{ route('users.remove_friend',  $user->id ) }}" method="post">
                            @csrf
                            <button type="submit" name="submit" class="btn btn-primary">Remove Friend</button>
                        </form>
                    </td>
                    <td scope='row'><a href="{{ route('conversations.check_conversation', $user->id ) }}" class="btn btn-primary">Send Message</a></td>
                </tr>   
             @endforeach
       </tbody>
    </table>
</div>

@endsection
