@extends('layouts.app')
@section('content')
<div class="container">
    <div class="text-center">
        <h3>Messages</h3>
    </div>
    @foreach ($conversation as $conv)
        @if ($conv->sender_id == Auth::user()->id)
        <div class="text-left p-2 bg-primary text-white">
               {{ Auth::user()->username }}:<br> {{ $conv->message }}
        </div>
        @else
        <div class="text-right p-2 bg-dark text-white">
                {{ $other_correspondent->username }}: <br> {{ $conv->message }}
        </div>
        @endif
        @endforeach
        <br>
        <form action="{{ route('users.send_message',  $conv->conversation_id) }}" method="post">
            @csrf
            <div class="form-group row">
                <label for="message" class="col-md-4 col-form-label text-md-right">{{ __('message') }}</label>
    
                <div class="col-md-6">
                    <input id="message" type="text" class="form-control @error('message') is-invalid @enderror" name="message" autofocus>
    
                    @error('message')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Send Message') }}
                    </button>
                </div>
            </div>
        </form>
</div>

@endsection
