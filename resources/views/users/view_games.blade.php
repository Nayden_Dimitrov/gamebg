@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">My games</h1>
    </div>
  
    <table class="table col-8 text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <tbody>
             @foreach($userGames as $game)
                <tr>
                    <td scope="row"><a href="/games/{{ $game->id }}">{{ $game->id }}</a></td>
                    <td scope="row"><a href="/games/{{ $game->id }}">{{ $game->name }}</a></td>

                    @if ($game->images->count())
                    <td scope="row"><img class ='img-fluid' src={{ asset('images/games/'.$game->getMainPic()) }} alt="{{ $game->name }}"> </td>
                    @else
                    <td scope="row">No data available.</td>
                    @endif
                    <td scope='row'>
                        @if ($game->usergame()->count())
                        <form action="{{ route('user_game.remove',  $game->id ) }}" method="post">
                            @csrf
                                <button type="submit" name="submit" class="btn btn-primary">I dont play this game anymore</button>
                        </form> 
                        @else
                        <form action="{{ route('user_game.store',  $game->id ) }}" method="post">
                            @csrf
                                <button type="submit" name="submit" class="btn btn-primary">I play this game</button>
                        </form>
                        @endif
                    </td>
                </tr>   
             @endforeach
       </tbody>
    </table>
</div>

@endsection
