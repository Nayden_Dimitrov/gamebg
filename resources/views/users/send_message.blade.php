@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">Send Message to {{ $receiver->username }}</h1>
    </div>
    <form action="{{ route('users.send_message',  $conversation) }}" method="post">
        @csrf
        <div class="form-group row">
            <label for="message" class="col-md-4 col-form-label text-md-right">{{ __('message') }}</label>

            <div class="col-md-6">
                <input id="message" type="text" class="form-control @error('message') is-invalid @enderror" name="message" autofocus>

                @error('message')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Send Message') }}
                </button>
            </div>
        </div>
    </form>
</div>

@endsection
