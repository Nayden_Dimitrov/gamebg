@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">{{ $user->username }} profile</h1>
    </div>
  
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Played Games </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td scope="row">{{ $user->id }}</td>
                <td scope="row">{{ $user->name }}</td>
                <td scope="row">
                    @forelse ($userGames as $game)
                    <a href="/games/{{ $game->id }}" class="text-decoration-none">{{ $game->name }}</a> <br>
                    @empty
                        User have no favourite games.
                    @endforelse 
                </td> 
                <td scope='row'>
                    @if ( Auth::user()->isAFriend($user->id))
                    <form action="{{ route('users.remove_friend',  $user->id ) }}" method="post">
                        @csrf
                        <button type="submit" name="submit" class="btn btn-primary">Remove Friend</button>
                    </form>
                    @else
                    <form action="{{ route('users.add_friend',  $user->id ) }}" method="post">
                        @csrf
                        <button type="submit" name="submit" class="btn btn-primary">Add Friend</button>
                    </form>
                    @endif
                </td>
            </tr>   
       </tbody>
    </table>
</div>

@endsection
