@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
                <th scope="col"> Search Buddy </th>
            </tr>
        </thead>
        <tbody>
              <tr>
                  <td scope="row">{{ $game->id }}</td>
                  <td scope="row">{{ $game->name }}</td>
                    <td scope="row">
                        @forelse ($gameImages as $gameImage)
                        <img class ='img-fluid' src={{ asset('images/games/'.$gameImage->image) }} alt="{{ $game->name }}">
                    @empty
                        No Data Available.
                  @endforelse
                    </td>
                  <td scope='row'>
                    @if ($game->usergame()->count())
                    <form action="{{ route('user_game.remove',  $game->id ) }}" method="post">
                        @csrf
                            <button type="submit" name="submit" class="btn btn-primary">I dont play this game anymore</button>
                    </form> 
                    @else
                    <form action="{{ route('user_game.store',  $game->id ) }}" method="post">
                        @csrf
                            <button type="submit" name="submit" class="btn btn-primary">I play this game</button>
                    </form>
                    @endif
                </td>
                <td><a href="{{ route('games.show_game',  $game->id ) }}" class="btn btn-primary">People who play this game</a></td>
              </tr>
       </tbody>
    </table>
</div>

@endsection
