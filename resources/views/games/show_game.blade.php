@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">Who Play {{ $game->name }} Game</h1>
    </div>
  
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <tbody>
            @forelse ($players as $player)
            <tr>
                <td scope="row"><a href="/users/{{ $player->id }}" class="text-decoration-none">{{ $player->id }}</a></td>
                <td scope="row"><a href="/users/{{ $player->id }}" class="text-decoration-none">{{ $player->name }}</a></td>
                <td scope="row"><img class ='img-fluid' src={{ $player->getImage()}} alt="{{$player->name}}"></td>
                <td scope='row'> @if ( Auth::user()->isAFriend($player->id))
                    <form action="{{ route('users.remove_friend',  $player->id ) }}" method="post">
                        @csrf
                        <button type="submit" name="submit" class="btn btn-primary">Remove Friend</button>
                    </form>
                    @else
                    <form action="{{ route('users.add_friend',  $player->id ) }}" method="post">
                        @csrf
                        <button type="submit" name="submit" class="btn btn-primary">Add Friend</button>
                    </form>
                    @endif</td>
            </tr>   
            @empty
                <td scope="row">No Current Users played this game.</td>
            @endforelse
       </tbody>
    </table>
</div>

@endsection
