@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/categories" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">All Games from {{ $nameOfCategory->category_name }} Category</h1>
    </div>
  
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
                <th scope="col">Search Buddy</th>
            </tr>
        </thead>
        <tbody>
             @foreach($games as $game)
                <tr>
                    <td scope="row"><a href="/games/{{ $game->id }}" class="text-decoration-none">{{ $game->id }}</a></td>
                    <td scope="row"><a href="/games/{{ $game->id }}" class="text-decoration-none">{{ $game->name }}</a></td>

                    @if ($game->images->count())
                    <td scope="row"><img class ='img-fluid' src={{ asset('images/games/'.$game->getMainPic()) }} alt="{{ $game->name }}"> </td>
                    @else
                    <td scope="row">No data available.</td>
                    @endif
                    <td scope='row'>
                        @if ($game->usergame()->count())
                        <form action="{{ route('user_game.remove',  $game->id ) }}" method="post">
                            @csrf
                                <button type="submit" name="submit" class="btn btn-primary">I dont play this game anymore</button>
                        </form> 
                        @else
                        <form action="{{ route('user_game.store',  $game->id ) }}" method="post">
                            @csrf
                                <button type="submit" name="submit" class="btn btn-primary">I play this game</button>
                        </form>
                        @endif
                    </td>
                    <td><a href="{{ route('games.show_game',  $game->id ) }}" class="btn btn-primary">People who play this game</a></td>
                </tr>   
             @endforeach
       </tbody>
    </table>
</div>

@endsection
