@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('GameBg') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ __('Welcome') }}, {{ $user->username }}! <br>
                    @if(Auth::check())
                        @if (Auth::user()->isAdmin())
                        <a href="{{ route('admin.index') }}" class="btn btn-primary">Enter Admin Panel</a>
                        @endif
                    @endif
                    <a href="/categories" class="btn btn-primary">Choose Game Category</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
