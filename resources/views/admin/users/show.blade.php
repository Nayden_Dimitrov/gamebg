@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="text-center mb-5 mt-3">User: {{ $user->name }} </h1>
    <a href="{{ route('admin.users.index') }}" class="btn btn-primary float-right">Back</a>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Username</th>
                <th scope="col"> Email</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td scope="row"><a href="{{$user->id}}" class="text-decoration-none">{{$user->id}}</a>  </td>
                <td scope="row"><a href="{{$user->id}}" class="text-decoration-none"> {{$user->name}}</a> </td>
                <td scope="row"><a href="{{$user->id}}" class="text-decoration-none"> {{$user->username}}</a> </td>
                <td scope="row"><a href="" class="text-decoration-none">{{ $user->email }}</a></td>
            </tr>
       </tbody>
    </table>
</div>

@endsection
