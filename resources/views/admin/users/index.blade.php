@extends('layouts.app')
@section('content')
<div class="container">
   
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">All Users</h1>
    </div>
    <a href="/admin" class="btn btn-primary float-right">Back</a>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> Username</th>
                <th scope="col"> Email</th>
                <th scope="col"> Number of playing games</th>
                <th scope="col"> Using the app</th>
                <th scope="col"> Action</th>
            </tr>
        </thead>
        <tbody>
             @foreach($users as $user)
                <tr>
                    <td scope="row"><a href="" class="text-decoration-none">{{ $user->username }}</a></td>
                    <td scope="row"><a href="" class="text-decoration-none">{{ $user->email }}</a></td>
                    <td scope="row"><a href="" class="text-decoration-none">{{ $user->count }}</a></td>
                    <td></td>
                    <td class="d-flex">
                        <a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-success">View</a>
                        <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>   
             @endforeach
       </tbody>
    </table>
    {{ $users->links() }}
</div>

@endsection
