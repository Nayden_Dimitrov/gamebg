@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="text-center mb-5 mt-3">All Game Categories</h1>
    <a href="{{ route('categories.create') }}" class="btn btn-success">Add New Category</a>
    <a href="/admin" class="btn btn-primary float-right">Back</a>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
             @foreach($categories as $category)
            <tr>
                <td scope="row"><a href="{{ route('categories.show', $category->id) }}" class="text-decoration-none">{{$category->id}}</a>  </td>
                <td scope="row"><a href="{{ route('categories.show', $category->id) }}" class="text-decoration-none"> {{$category->category_name}}</a> </td>
                <td scope="row"><img class="img-fluid" src={{ asset('images/categories/'.$category->image) }} alt="{{$category->category_name}}"> </td>
                <td class='d-flex'>
                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success">Edit</a>
                    <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
             @endforeach
       </tbody>
    </table>
    {{ $categories->links() }}
</div>

@endsection
