@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{ route('categories.index') }}" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">{{ $category->name }}</h1>
    </div>
  
    <form method="POST" action="{{ route('categories.update', $category) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="category_name" class="col-md-4 col-form-label text-md-right">{{ __('Category Name') }}</label>

            <div class="col-md-6">
                <input id="category_name" type="text" class="form-control @error('category_name') is-invalid @enderror" name="category_name" value="{{ $category->category_name }}" required autocomplete="category_name" autofocus>

                @error('category_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>

            <div class="col-md-6">                       
            <img class ='img-fluid' src={{ $category->getImage() }} alt="{{$category->image}}">

                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Change Image') }} 
            </label>
            <div class="col-md-6">
                <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image">

                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update Category') }}
                </button>
            </div>
        </div>
    </form>
</div>

@endsection
