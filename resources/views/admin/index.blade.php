@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="text-center mb-5 mt-3">Welcome, admin</h3>
    <div class="row justify-content-around">
        <div class="col text-center">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('categories.index') }}" class="btn btn-primary">Show All Categories</a>
                    <a href="{{ route('games.index') }}" class="btn btn-primary">Show All Games</a>
                    <a href="{{ route('admin.users.index') }}" class="btn btn-primary">Show All Users</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
