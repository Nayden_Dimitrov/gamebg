@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ route('games.index') }}" class="btn btn-primary float-right">Back</a>
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">{{ $game->name }} Game</h1>
    </div>
  
    <form method="POST" action="{{ route('admin.game.update', $game) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __(' Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $game->name }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>
            <div class="col-md-6">               
                @forelse ($gameImages as $gameImage)
                    <img class ='img-fluid' src={{ asset('images/games/'.$gameImage->image) }} alt="{{ $game->name }}">
                    <label class="col-md-4 col-form-label text-md-right">{{ __('Main Pic') }}</label> 
                    <input type="radio" name="main_pic" value="{{ $gameImage->id }}" {{ $selected_main_pic == $gameImage->id ? "checked='checked'" : '' }} >
                    @if (!$gameImage->isMainPic())
                    <button type="button" name="delete_button" value="{{ $gameImage->id }}" class="btn btn-danger delete_image_button" data-delete_route="{{ route('admin.game.destroyImages', $gameImage->id) }}" >Delete</button>
                    @endif
                <input id="image" type="file" name="image[]" class="form-control @error('image') is-invalid @enderror" value='{{ $gameImage->id }}'>
                @empty
                        No Data Available.
                @endforelse
            </div>
        </div>
        
        <div class="form-group row">
            <label for="new_image" class="col-md-4 col-form-label text-md-right">{{ __('Upload new Image') }} 
            </label>
            <div class="col-md-6">
                <input id="new_image" type="file" multiple class="form-control @error('new_image') is-invalid @enderror" name="new_image[]">

                @error('new_image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update Game') }}
                </button>
            </div>
        </div>
    </form>
    <form action="" method="POST" id="delete_image_form" style="display:none;">
        @method('DELETE')
        @csrf
    </form>
</div>

@endsection

@section('scripts')
<script
src="https://code.jquery.com/jquery-3.5.1.min.js"
integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
crossorigin="anonymous">
</script>
<script>
    $(document).ready(function(){
        $('.delete_image_button').click(function(){
            let delete_route = $(this).data('delete_route');
            $('#delete_image_form').attr('action', delete_route);
            $('#delete_image_form').submit();
        })
    })
</script>
@endsection