@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{ route('games.index') }}" class="btn btn-primary float-right">Back</a>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <tbody>
              <tr>
                  <td scope="row">{{ $game->id }}</td>
                  <td scope="row">{{ $game->name }}</td>
                    <td scope="row">
                        @forelse ($gameImages as $gameImage)
                        <img class ='img-fluid' src={{ asset('images/games/'.$gameImage->image) }} alt="{{ $game->name }}">
                    @empty
                        No Data Available.
                  @endforelse
                    </td>
                  <td scope='row'>
                    <a href="{{ route('admin.game.edit', $game->id) }}" class="btn btn-success">Edit</a>
                </td>
              </tr>
       </tbody>
    </table>
</div>

@endsection
