@extends('layouts.app')
@section('content')
<div class="container">
   
    <div class="d-flex mb-5 mt-3 justify-content-around">
        <h1 class="text-center">All Games</h1>
    </div>
    <a href="{{ route('admin.game.create') }}" class="btn btn-success">Add New Game</a>
    <a href="/admin" class="btn btn-primary float-right">Back</a>
    <form  id="myform" action="{{ route('games.index') }}" method="get">
    <div class="form-group row">
        <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Category') }} </label>
            <div class="col-md-6">
                <select class="form-control" name="category" id="category" onchange='this.form.submit()'>
                    <option selected="selected" value=''>All</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{$selected_category == $category->id ? 'selected' : '' }}>{{ $category->category_name }}</option>
                    @endforeach
                </select>
            </div>
    </div>
</form>
    <table class="table col text-center" align="center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> id</th>
                <th scope="col"> Name</th>
                <th scope="col"> Image </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <tbody>
             @foreach($games as $game)
                <tr>
                    <td scope="row"><a href="{{ route('admin.game.show',  $game->id ) }}" class="text-decoration-none">{{ $game->id }}</a></td>
                    <td scope="row"><a href="{{ route('admin.game.show',  $game->id ) }}" class="text-decoration-none">{{ $game->name }}</a></td>

                    @if ($game->images->count())
                    <td scope="row"><img class ='img-fluid' src={{ asset('images/games/'.$game->getMainPic()) }} alt="{{ $game->name }}"> </td>
                    @else
                    <td scope="row">No data available.</td>
                    @endif
                    <td class="d-flex">
                        <a href="{{ route('admin.game.edit', $game->id) }}" class="btn btn-success">Edit</a>
                        <form action="{{ route('admin.game.destroy', $game->id) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>   
             @endforeach
       </tbody>
    </table>
</div>

@endsection
