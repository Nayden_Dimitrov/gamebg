<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Game::class, function (Faker $faker) {
    $categories = Category::pluck('id')->toArray();
    //    $sourceDir = public_path('games_img/');
    //     $targetDir = public_path('images/games/');
    //     $imageName = $faker->file($sourceDir, $targetDir, false);

    return [
        'name' => $faker->unique()->name,
        'category_id' => $faker->randomElement($categories),
    ];
});
