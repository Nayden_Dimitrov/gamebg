<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\GameImage;
use Faker\Generator as Faker;

$factory->define(GameImage::class, function (Faker $faker) {
    $games = Game::pluck('id')->toArray();
    $sourceDir = public_path('games_img/');
    $targetDir = public_path('images/games/');
    $imageName = $faker->file($sourceDir, $targetDir, false);

 return [
     'game_id' => $faker->randomElement($games),
     'isMainPic' => 0,
     'image' => $imageName,
 ];
});
