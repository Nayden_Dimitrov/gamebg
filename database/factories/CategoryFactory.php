<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    
        $sourceDir = public_path('category_img/');
        $targetDir = public_path('images/categories/');
        $imageName = $faker->file($sourceDir, $targetDir, false);
 
     return [
         'category_name' => $faker->unique()->name,
         'image' => $imageName
    ];
});
