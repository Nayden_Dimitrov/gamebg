<?php

use App\Game;
use App\GameImage;
use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Game::class, 100)->create()->each(function ($game) {
            factory(GameImage::class)->create([
                'game_id' => $game->id,
                'isMainPic' => 1
            ]);
            factory(GameImage::class, 3)->create([
                'game_id' => $game->id
            ]);
        });
    }
}
