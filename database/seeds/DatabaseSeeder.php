<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
  
        $this->call(UserTableSeeder::class);
        factory(App\User::class, 20)->create();
        factory(App\Category::class, 10)->create();
        $this->call(GamesTableSeeder::class);
    }
}
