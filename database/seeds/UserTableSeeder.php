<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(User::class, 20)->create()->each(function ($user) {
        });

        factory(User::class)->create([
            'username' => 'Test User',
            'name' => 'Nayden',
            'email' => 'ndimitrov@weband.bg',
            'password' => Hash::make('123123'),
            'avatar' => '/images/default.png',
            'remember_token' => null,
            'admin' => '1',
        ]);
        
    }
}
