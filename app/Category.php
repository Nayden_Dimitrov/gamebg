<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    public function games()
    {
        return $this->hasMany(Game::class);
    }

    public function getImage()
    {
        return asset('images/categories/' . $this->image);
    }

    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('123')) . '.' . $image->getClientOriginalExtension();
    }
}
