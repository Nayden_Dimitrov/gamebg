<?php

namespace App;

use App\Game;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'username', 'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImage()
    {
        if ( $this->avatar != '/images/default.png'){
            return asset('images/users/' . $this->avatar);
        } 
        
        return asset('images/default.png');
    }

    public function games()
    {
        return $this->belongsToMany(Game::class);
    }

    public function friends()
	{
		return $this->belongsToMany(User::class, 'friend_user', 'user_id', 'friend_id');
    }

    public function addFriend(User $user)
	{
        $this->friends()->attach($user);
        $user->friends()->attach($this);
	}

	public function removeFriend(User $user)
	{
        $this->friends()->detach($user);
        $user->friends()->detach($this);
    }

    public function isAFriend($friend_id)
    {
        $isAFriend = DB::table('friend_user')->where('user_id', $friend_id)
        ->where('friend_id', $this->id)->orWhere('friend_id', $friend_id)->where('user_id', $this->id)->first();
        return $isAFriend;
    }
    
    public function isAdmin()
    {
        return $this->admin;
    }

}
