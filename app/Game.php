<?php

namespace App;

use App\GameImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $guarded = [];
    public function category() 
    {
        return $this->belongsTo(Category::class,'category_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(GameImage::class);
    }

    public function getMainPic()
    {
        $mainImage = GameImage::where('game_id', $this->id)->where('isMainPic', '=', 1)->first('image');
        return $mainImage->image;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function userGame()
    {
        $user = Auth::user();
        $userGame = DB::table('game_user')->where('game_id', $this->id)->where('user_id', $user->id)->get();
        return $userGame;
    }

    public function getCurrentId(){
        return $this->id;
    }
 
}
