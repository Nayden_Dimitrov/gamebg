<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameImage extends Model
{
    protected $guarded = [];
    
    public function game() 
    {
        return $this->belongsTo(Game::class,'game_id', 'id');
    }

    public function isMainPic()
    {
        $isMainPic = GameImage::where('id', $this->id)->where('isMainPic', '=', 1)->first();
        return $isMainPic;
    }
}
