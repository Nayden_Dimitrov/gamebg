<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    // Relations
    public function userOne()
    {
        return $this->belongsTo(User::class,'user_one');
    }

    public function userTwo()
    {
        return $this->belongsTo(User::class,'user_two');
    }

    public function getOtherCorrespondent(User $user)
    {
        if($this->userOne->is($user))
        {
            return $this->userTwo;
        }
        return $this->userOne;
    }
}
