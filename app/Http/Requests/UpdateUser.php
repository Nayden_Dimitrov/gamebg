<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Auth;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

            return [
                'username' => 'required|string|unique:users,username,'. $user->id . '|max:255',
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users,email,'. $user->id . '|max:255',  
                'avatar' => 'sometimes|image|mimes:jpg,jpeg,png|max:5000'
            ];
    }
}
