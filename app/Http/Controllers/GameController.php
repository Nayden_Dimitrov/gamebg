<?php

namespace App\Http\Controllers;

use App\Game;
use App\Category;
use App\GameImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {   
        $category = request('cat');
        $nameOfCategory = Category::where('id', $category)->first();
        $games = Game::where('category_id', $category)->get();
        return view('games.index', compact('games','nameOfCategory'));
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        $games = request()->segment(count(request()->segments()));
        $gameImages = DB::table('game_images')->where('game_id', $games)->get();
        return view('games.show', compact('game', 'gameImages'));
    }

    public function showGame($game_id)
    {
        $user = Auth::user();
        $game = Game::find($game_id);
        $players = $game->users()->where('user_id', '!=', $user->id)->orderBy('id')->get();
        return view('games.show_game', compact('players','game', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        //
    }
}
