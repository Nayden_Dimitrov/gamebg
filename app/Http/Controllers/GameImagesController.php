<?php

namespace App\Http\Controllers;

use App\GameImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GameImages  $gameImages
     * @return \Illuminate\Http\Response
     */
    public function show(GameImages $gameImages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GameImages  $gameImages
     * @return \Illuminate\Http\Response
     */
    public function edit(GameImages $gameImages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GameImages  $gameImages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GameImages $gameImages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GameImages  $gameImages
     * @return \Illuminate\Http\Response
     */
    public function destroy(GameImages $gameImages)
    {
        //
    }
}
