<?php

namespace App\Http\Controllers\Admin;


use App\User;
use App\Message;
use App\Category;
use App\Conversation;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUser;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use App\Http\Requests\UpdatePassword;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\Exists;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $categories = Category::all();
        
        return view('admin.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = DB::table('users')->where('id', $id)->first();
        $user = User::where('id', $id)->first();
        $userGames = $user->games()->orderBy('id')->get();
        return view('users.show', compact('user', 'userGames'));
    }

    public function editProfile ()
    {
        $user = Auth::user();
        return view ('users.update_profile', compact('user'));
    }

    public function updateProfile ( UpdateUser $request)
    {
        $user = Auth::user();

        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->hasFile('avatar')) {
            $avatarPath = public_path('/images/users/');
            $picturePath = $avatarPath.$user->avatar;

            if (file_exists($picturePath)) {
                unlink($picturePath);
            }
            $avatarUploaded = request()->file('avatar');
            $avatarName = time() . '.' . $avatarUploaded->getClientOriginalExtension();
            $avatarPath = public_path('/images/users/');
            $avatarUploaded->move($avatarPath,$avatarName);
            $user->avatar = $avatarName;
        }

        $user->save();
        
        return redirect()->back()->with('success', 'Successful changes on your profile');;
    }

    public function updatePassword(UpdatePassword $request )
    {
        
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        
        return redirect()->back()->with('success', 'Password is succesfully changed');

    }

    public function addGame($game_id)
    {
        $user = Auth::user();

        $user->games()->attach($game_id);

        return redirect()->back()->with('success', 'The Game Is successfuly added');
    }

    public function removeGame($game_id)
    {
        $user = Auth::user();

        $user->games()->detach($game_id);

        return redirect()->back()->with('success', 'The Game Is successfuly removed');
    }

    public function viewGames()
    {
        $user = Auth::user();

        $userGames = $user->games()->orderBy('id')->get();

        return view('users.view_games', compact('userGames'));
    }

    public function viewFriends()
    {
        $user = Auth::user();

        $userFriends = $user->friends()->orderBy('id')->get();

        return view('users.view_friends', compact('userFriends', 'user'));
    }

    
	public function getaddFriend(User $user)
	{
        Auth::user()->addFriend($user);
        return redirect()->back()->with('success', 'The User Is successfuly added');
    }
    
	public function getremoveFriend(User $user)
	{
        Auth::user()->removeFriend($user);
        return redirect()->back()->with('success', 'The User Is successfuly removed');
    }

    public function sendMessage($conversationId, Request $request)
    {
        $validatedData = $request->validate([
            'message' => ['required'],
        ]);
       
        $user = Auth::user();
        $message = new Message();
        $message->conversation_id = $conversationId;
        $message->sender_id = Auth::user()->id;
        $message->message = $request->message;
        $message->save();
        $messages = Message::where('conversation_id', $conversationId)->get();
        $other_correspondent_in_conversation = Message::where('conversation_id', $conversationId)->where('sender_id', '!=', $user->id)->first();
        if($other_correspondent_in_conversation){
            $recieverId = $other_correspondent_in_conversation->sender_id;
            $other_correspondent = User::where('id', $recieverId)->first();
        } else {
            $other_correspondent = '';
        }
        
        return redirect()->back()->with('success', 'The Message is successfully send!');
    }

    public function viewAllConversations()
    {
        $user = Auth::user();

        $allConversations = Conversation::where('user_one', '=', $user->id)->orWhere('user_two', '=', $user->id )->get();

        return view('users.view_all_conversations', compact('user', 'allConversations'));
    }

    public function viewCurrentConversation($conversationId)
    {

        $user = Auth::user();
        $conversation = Message::where('conversation_id', $conversationId)->get();

        $other_correspondent_in_conversation = Message::where('conversation_id', $conversationId)->where('sender_id', '!=', $user->id)->first();
        if($other_correspondent_in_conversation){
            $recieverId = $other_correspondent_in_conversation->sender_id;
            $other_correspondent = User::where('id', $recieverId)->first();
        } else {
            $other_correspondent = '';
        }

        return view('users.view_current_conversation', compact('conversation', 'other_correspondent'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
