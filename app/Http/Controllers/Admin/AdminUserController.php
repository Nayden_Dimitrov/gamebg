<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\User;
use App\Category;
use App\GameImage;
use App\GameImages;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminUserController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->paginate(10);
        foreach($users as $user){
            $userGames = DB::table('game_user')->where('user_id', $user->id)->get();
            $countGames = $userGames->count();
            $user->count = $countGames;
            
        }
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('weband')) . '.' . $image->getClientOriginalExtension();
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|string|unique:games|max:255',
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);
           
       $game = Game::create([
            'name' =>  $request->name,
            'category_id' => $request->category,
        ]);
        
        if($request->has('image'))
        {
            $image = $request->image;

            foreach ($image as $key => $img){
                $image_name = $this->hashImageName($img);
                if($key == 0){
                    GameImage::create([
                        'game_id' => $game->id,
                        'image' => $image_name,
                        'isMainPic' => 1
                    ]);
                } else {
                    GameImage::create([
                        'game_id' => $game->id,
                        'image' => $image_name,
                        'isMainPic' => 0
                    ]);
                }
                Storage::disk('images')->putFileAs('games', $img, $image_name);
            }
        }
        return redirect('/admin/games')->with('success', 'Successful created a game');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       $user = User::where('id', $id)->first();

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = Game::where('id', $id)->first();
        $gameImages = GameImage::where('game_id', $id)->where('game_id', $id)->get();
        $getMainPic = GameImage::where('game_id', $id)->where('isMainPic', '=', '1')->first();

        $selected_main_pic = $getMainPic->id;
        
        return view('admin.games.edit', compact('game', 'gameImages', 'selected_main_pic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $game = Game::where('id', $id)->first();
        $gameImages = GameImage::where('game_id', $id)->get();

        //delete single picture
        if($request->input('delete_button')){
            $gameImageId = $request->delete_button;
            $gameImage = GameImage::where('id', $gameImageId)->first();
        
            if (Storage::disk('images')->exists('games/'. $gameImage->image)) {
                Storage::disk('images')->delete('games/'. $gameImage->image);
            }

        GameImage::where('id', $gameImageId)->delete();
        return redirect('/admin/games')->with('success', 'Successful delete game picture');

        }
        //Change main pic
        foreach ($gameImages as $gameImage) {
            if($request->main_pic != $gameImage->id) {
                $gameImage->isMainPic = 0;
                $gameImage->save();
                GameImage::where('game_id', $id)->where('id', '=', $request->main_pic)->update(['isMainPic' => 1]);
            }
        }
        
        if($request->has('image'))
        {
           
            $old_image_id = $request->image;
            
            //REMOVE old image
            foreach($old_image_id as $key => $value) {
                $old_image_id = $key;
            }

            foreach ($gameImages as $key => $gameImage) {
                if($key == $old_image_id){
                    if (Storage::disk('images')->exists('games/'. $gameImage->image)) {
                        Storage::disk('images')->delete('games/'. $gameImage->image);
                        GameImage::where('id', $gameImage->id)->delete();
                    }
                }
            }

            //INPUT NEW IMAGE
            $image = $request->image;

            foreach ($image as $key => $img){
                
                $image_name = $this->hashImageName($img);
                    GameImage::create([
                        'game_id' => $game->id,
                        'image' => $image_name,
                        'isMainPic' => 0
                    ]);
                Storage::disk('images')->putFileAs('games', $img, $image_name);
            }
            return redirect('/admin/games')->with('success', 'Successful updated a game');
        }  

        if($request->has('new_image'))
        {
            $image = $request->new_image;
        
            foreach ($image as $key => $img){
                $image_name = $this->hashImageName($img);
                    GameImage::create([
                        'game_id' => $game->id,
                        'image' => $image_name,
                        'isMainPic' => 0
                    ]);
                Storage::disk('images')->putFileAs('games', $img, $image_name);
            }
        }
        return redirect('/admin/games')->with('success', 'Successful updated a game');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $user = User::where('id', $id)->first();

            if (Storage::disk('images')->exists('users/'. $user->avatar)) {
                Storage::disk('images')->delete('users/'. $user->avatar);
            }

        User::where('id', $id)->delete();
       
        return redirect('/admin/users')->with('success', 'Successful delete a user');

    }

}
