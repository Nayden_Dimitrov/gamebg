<?php

namespace App\Http\Controllers\Admin;


use App\User;
use App\Image;
use App\Message;
use App\Category;
use App\Conversation;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUser;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use App\Http\Requests\UpdateCategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\Exists;

class AdminCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $categories = DB::table('categories')->paginate(5);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_name' => 'required|string|unique:categories|max:255',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:5000'
        ]);

        if($request->hasFile('image') && $request->image->isValid()) {
            
            $image = $request->image;
            $image_name = $this->hashImageName($image);
            
            //store the image in the filesystem
            Storage::disk('images')->putFileAs('categories', $image, $image_name);
            
            //store the path as text in the database
            Category::create([
                'category_name' =>  $request->category_name,
                'image' =>  $image_name, 
            ]);

            return redirect('/admin/categories')->with('success', 'Successful create a category');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = DB::table('users')->where('id', $id)->first();
        $category = Category::where('id', $id)->first();
        return view('admin.categories.show', compact('category'));
    }
   

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($categoryId)
    {
        $category = Category::where('id', $categoryId)->first();

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('weband')) . '.' . $image->getClientOriginalExtension();
    }

    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();
        
        $request->validate([
            'category_name' => 'required|string|unique:categories,category_name,'. $category->id . '|max:255',
            'image' => 'sometimes|image|mimes:jpg,jpeg,png|max:5000'
        ]);

        $category->category_name = $request->category_name;

        if($request->hasFile('image') && $request->image->isValid()) {

            $imagePath = public_path('/images/categories/');
            $picturePath = $imagePath.$category->image;
            if (file_exists($picturePath)) {
                Storage::disk('images')->delete('categories/'. $category->image);
            }
            
            $image = $request->image;
            $image_name = $this->hashImageName($image);
            
            //store the image in the filesystem
            Storage::disk('images')->putFileAs('categories', $image, $image_name);
            
            //store the path as text in the database

            $category->image = $image_name;
        }

        $category->save();
        return redirect()->back()->with('success', 'Successful change the category');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id', $id)->delete();
        return redirect('/admin/categories')->with('success', 'Successful delete a category');

    }
}
